<?php

declare(strict_types = 1);

namespace App\Application\Query\GetUserById;

use App\Domain\User\User;

final class GetUserByIdResponse
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}