<?php

declare(strict_types = 1);

namespace App\Application\Query\GetUserById;

use App\Domain\Exception\EntityNotFoundException;
use App\Domain\Exception\ValidationException;
use App\Domain\User\UserId;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepository;

final class GetUserByIdQueryHandler
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function execute(GetUserByIdQuery $query): GetUserByIdResponse
    {
        try {
            $userId = UserId::fromString($query->getUserId());
            $user = $this->userRepository->getById($userId);
        } catch (EntityNotFoundException $ex) {
            throw UserNotFoundException::forId($query->getUserId());
        } catch (\InvalidArgumentException $ex) {
            throw ValidationException::invalidValue($ex->getMessage());
        }

        return new GetUserByIdResponse($user);
    }
}