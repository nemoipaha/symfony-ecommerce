<?php

declare(strict_types = 1);

namespace App\Application\Query\GetUsers;

use App\Domain\Exception\ValidationException;
use App\Domain\User\UserRepository;
use App\Domain\ValueObject\Limit;

final class GetUsersQueryHandler
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function execute(GetUsersQuery $getUsersQuery): GetUsersResponse
    {
        try {
            $limit = Limit::default();

            if ($getUsersQuery->getLimit() !== null) {
                $limit = Limit::fromString($getUsersQuery->getLimit());
            }
        } catch (\InvalidArgumentException $ex) {
            throw ValidationException::invalidValue($ex->getMessage());
        }

        $users = $this->userRepository->findByFilters('email', 'desc', $limit->value());

        return new GetUsersResponse($users);
    }
}