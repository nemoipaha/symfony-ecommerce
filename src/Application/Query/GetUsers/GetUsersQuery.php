<?php

declare(strict_types = 1);

namespace App\Application\Query\GetUsers;

final class GetUsersQuery
{
    private $limit;

    public function __construct(?string $limit)
    {
        $this->limit = $limit;
    }

    public function getLimit(): ?string
    {
        return $this->limit;
    }
}