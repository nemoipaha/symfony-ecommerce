<?php

declare(strict_types = 1);

namespace App\Application\Query\GetUsers;

use Doctrine\Common\Collections\ArrayCollection;

final class GetUsersResponse
{
    private $users;

    public function __construct(ArrayCollection $users)
    {
        $this->users = $users;
    }

    public function getUsers(): ArrayCollection
    {
        return $this->users;
    }
}