<?php

declare(strict_types=1);

namespace App\Application\Command\ForgotPassword;

final class ForgotPasswordCommand
{
    private $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}
