<?php

declare(strict_types=1);

namespace App\Application\Command\ForgotPassword;

use App\Domain\Auth\PasswordToken;
use App\Domain\Auth\PasswordTokenRepository;
use App\Domain\Auth\ResetPasswordEvent;
use App\Domain\Exception\EntityNotFoundException;
use App\Domain\User\EmailAddress;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class ForgotPasswordCommandHandler
{
    private $userRepository;
    private $dispatcher;
    private $passwordTokenRepository;

    public function __construct(
        UserRepository $userRepository,
        EventDispatcherInterface $dispatcher,
        PasswordTokenRepository $passwordTokenRepository
    ) {
        $this->userRepository = $userRepository;
        $this->dispatcher = $dispatcher;
        $this->passwordTokenRepository = $passwordTokenRepository;
    }

    public function execute(ForgotPasswordCommand $command): void
    {
        $email = EmailAddress::fromString($command->getEmail());

        try {
            $user = $this->userRepository->findByEmail($email);
        } catch (EntityNotFoundException $exception) {
            throw UserNotFoundException::forEmail((string)$email);
        }

        $token = PasswordToken::generate($user->getEmailAddress());

        $this->passwordTokenRepository->store($token);

        $this->dispatcher->dispatch(
            ResetPasswordEvent::NAME,
            new ResetPasswordEvent((string)$token, (string)$user->getEmailAddress())
        );
    }
}
