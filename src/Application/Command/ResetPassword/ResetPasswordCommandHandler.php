<?php

declare(strict_types=1);

namespace App\Application\Command\ResetPassword;

use App\Domain\Auth\InvalidPasswordToken;
use App\Domain\Auth\PasswordConfirmationInvalid;
use App\Domain\Auth\PasswordTokenRepository;
use App\Domain\Exception\EntityNotFoundException;
use App\Domain\Service\Security\PasswordEncoderInterface;
use App\Domain\User\EmailAddress;
use App\Domain\User\Password;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepository;

final class ResetPasswordCommandHandler
{
    private $userRepository;
    private $passwordEncoder;
    private $passwordTokenRepository;

    public function __construct(
        UserRepository $userRepository,
        PasswordEncoderInterface $passwordEncoder,
        PasswordTokenRepository $passwordTokenRepository
    ) {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->passwordTokenRepository = $passwordTokenRepository;
    }

    public function execute(ResetPasswordCommand $command): void
    {
        if ($command->getPassword() !== $command->getPasswordConfirmation()) {
            throw PasswordConfirmationInvalid::make();
        }

        $password = Password::generate($this->passwordEncoder, $command->getPassword());
        $email = EmailAddress::fromString($command->getEmail());

        try {
            $user = $this->userRepository->findByEmail($email);
        } catch (EntityNotFoundException $ex) {
            throw UserNotFoundException::forEmail($command->getEmail());
        }

        $token = $this->passwordTokenRepository->findByEmail($email);

        if (! $token->isHashMatches($command->getToken())) {
            throw InvalidPasswordToken::make();
        }

        $user = $user->withPassword($password);

        $this->userRepository->save($user);
    }
}
