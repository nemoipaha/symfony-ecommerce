<?php

declare(strict_types=1);

namespace App\Application\Command\ResetPassword;

final class ResetPasswordCommand
{
    private $email;
    private $token;
    private $password;
    private $passwordConfirmation;

    public function __construct(string $email, string $token, string $password, string $passwordConfirmation)
    {
        $this->email = $email;
        $this->token = $token;
        $this->password = $password;
        $this->passwordConfirmation = $passwordConfirmation;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getPasswordConfirmation(): string
    {
        return $this->passwordConfirmation;
    }
}
