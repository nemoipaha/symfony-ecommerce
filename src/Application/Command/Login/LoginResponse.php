<?php

declare(strict_types = 1);

namespace App\Application\Command\Login;

final class LoginResponse
{
    private $token;
    private $expiration;

    public function __construct(string $token, int $expiration)
    {
        $this->token = $token;
        $this->expiration = $expiration;
    }

    public function getExpiration(): int
    {
        return $this->expiration;
    }

    public function getToken(): string
    {
        return $this->token;
    }
}