<?php

declare(strict_types = 1);

namespace App\Application\Command\Login;

use App\Domain\Exception\EntityNotFoundException;
use App\Domain\Exception\InvalidUserCredentialsException;
use App\Domain\Exception\ValidationException;
use App\Domain\Service\Security\PasswordEncoderInterface;
use App\Domain\Service\Security\TokenGeneratorInterface;
use App\Domain\User\EmailAddress;
use App\Domain\User\User;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepository;

final class LoginCommandHandler
{
    private $userRepository;
    private $tokenGenerator;
    private $passwordEncoder;

    public function __construct(
        UserRepository $userRepository,
        PasswordEncoderInterface $passwordEncoder,
        TokenGeneratorInterface $tokenGenerator
    ) {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->tokenGenerator = $tokenGenerator;
    }

    public function execute(LoginCommand $command): LoginResponse
    {
        try {
            $user = $this->userRepository->findByEmail(
                EmailAddress::fromString($command->getEmail())
            );
        } catch (EntityNotFoundException $ex) {
            throw UserNotFoundException::forEmail($command->getEmail());
        } catch (\InvalidArgumentException $ex) {
            throw ValidationException::invalidValue($ex->getMessage());
        }

        $this->assertCredentials($user, $command->getPassword());

        $expiration = time() + 3600;
        $token = $this->tokenGenerator->generate($user, $expiration);

        return new LoginResponse($token, $expiration);
    }

    private function assertCredentials(User $user, string $passwordPlain): void
    {
        if (! $user->verifyPassword($this->passwordEncoder, $passwordPlain)) {
            throw InvalidUserCredentialsException::create();
        }
    }
}