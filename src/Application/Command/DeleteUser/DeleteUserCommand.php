<?php

declare(strict_types=1);

namespace App\Application\Command\DeleteUser;

final class DeleteUserCommand
{
    private $userId;

    public function __construct(string $userId)
    {
        $this->userId = $userId;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }
}