<?php

declare(strict_types=1);

namespace App\Application\Command\DeleteUser;

use App\Domain\Exception\EntityNotFoundException;
use App\Domain\Exception\ValidationException;
use App\Domain\User\UserId;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepository;

final class DeleteUserCommandHandler
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function execute(DeleteUserCommand $command): void
    {
        try {
            $id = UserId::fromString($command->getUserId());

            $user = $this->userRepository->getById($id);
        } catch (\InvalidArgumentException $exception) {
            throw ValidationException::invalidValue($exception->getMessage());
        } catch (EntityNotFoundException $exception) {
            throw UserNotFoundException::forId($command->getUserId());
        }

        $this->userRepository->delete($user);
    }
}