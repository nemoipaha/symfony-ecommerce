<?php

declare(strict_types = 1);

namespace App\Application\Command\SignUp;

final class SignUpCommand
{
    private $name;
    private $password;
    private $email;

    public function __construct(string $name, string $password, string $email)
    {
        $this->name = $name;
        $this->password = $password;
        $this->email = $email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getName(): string
    {
        return $this->name;
    }
}