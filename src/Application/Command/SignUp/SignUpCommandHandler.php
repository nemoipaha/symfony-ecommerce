<?php

declare(strict_types = 1);

namespace App\Application\Command\SignUp;

use App\Domain\Exception\ValidationException;
use App\Domain\Service\Security\PasswordEncoderInterface;
use App\Domain\User\EmailAddress;
use App\Domain\User\EmailAlreadyTakenException;
use App\Domain\User\Name;
use App\Domain\User\Password;
use App\Domain\User\User;
use App\Domain\User\UserRepository;
use App\Domain\User\UserSignedUpEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class SignUpCommandHandler
{
    private $userRepository;
    private $passwordEncoder;
    private $dispatcher;

    public function __construct(
        UserRepository $userRepository,
        PasswordEncoderInterface $passwordEncoder,
        EventDispatcherInterface $dispatcher
    ) {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->dispatcher = $dispatcher;
    }

    public function execute(SignUpCommand $command): SignUpResponse
    {
        try {
            $name = Name::create($command->getName());
            $password = Password::create(
                $this->passwordEncoder->encode($command->getPassword())
            );
            $email = EmailAddress::fromString($command->getEmail());
        } catch (\InvalidArgumentException $ex) {
            throw ValidationException::invalidValue($ex->getMessage());
        }

        if (! $this->userRepository->emailIsUnique($email)) {
            throw EmailAlreadyTakenException::forEmail($command->getEmail());
        }

        $user = new User(
            $this->userRepository->nextId(),
            $name,
            $password,
            $email
        );

        $this->userRepository->save($user);

        $this->dispatcher->dispatch(UserSignedUpEvent::NAME, new UserSignedUpEvent($user));

        return new SignUpResponse($user);
    }
}