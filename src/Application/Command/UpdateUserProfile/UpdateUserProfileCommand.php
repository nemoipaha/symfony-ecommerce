<?php

declare(strict_types=1);

namespace App\Application\Command\UpdateUserProfile;

final class UpdateUserProfileCommand
{
    private $email;
    private $name;
    private $userId;

    public function __construct(string $userId, string $email, string $name)
    {
        $this->email = $email;
        $this->name = $name;
        $this->userId = $userId;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getName(): string
    {
        return $this->name;
    }
}