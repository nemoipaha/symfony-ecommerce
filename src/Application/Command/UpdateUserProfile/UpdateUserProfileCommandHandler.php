<?php

declare(strict_types=1);

namespace App\Application\Command\UpdateUserProfile;

use App\Domain\Exception\EntityNotFoundException;
use App\Domain\Exception\ValidationException;
use App\Domain\User\EmailAddress;
use App\Domain\User\EmailAlreadyTakenException;
use App\Domain\User\Name;
use App\Domain\User\User;
use App\Domain\User\UserId;
use App\Domain\User\UserNotFoundException;
use App\Domain\User\UserRepository;

final class UpdateUserProfileCommandHandler
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function execute(UpdateUserProfileCommand $command): UpdateUserProfileResponse
    {
        try {
            $userId = UserId::fromString($command->getUserId());
            $email = EmailAddress::fromString($command->getEmail());
            $name = Name::create($command->getName());

            $user = $this->userRepository->getById($userId);
        } catch (\InvalidArgumentException $exception) {
            throw ValidationException::invalidValue($exception->getMessage());
        } catch (EntityNotFoundException $exception) {
            throw UserNotFoundException::forId($command->getUserId());
        }

        $this->assertEmailIsUnique($user, $email);

        $user = $user->withEmail($email);
        $user = $user->withName($name);

        $this->userRepository->save($user);

        return new UpdateUserProfileResponse($user);
    }

    private function assertEmailIsUnique(User $user, EmailAddress $emailAddress): void
    {
        try {
            $userByEmail = $this->userRepository->findByEmail($emailAddress);
        } catch (EntityNotFoundException $exception) {
            return;
        }

        if (! $userByEmail->getUid()->equals($user->getUid())) {
            throw EmailAlreadyTakenException::forEmail($emailAddress->value());
        }
    }
}