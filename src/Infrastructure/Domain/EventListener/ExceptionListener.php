<?php

declare(strict_types = 1);

namespace App\Infrastructure\Domain\EventListener;

use App\Domain\Exception\DomainExceptionInterface;
use App\Domain\Exception\EntityNotFoundException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        // todo create own AuthenticationException
        if ($exception instanceof HttpException && $exception->getStatusCode() === Response::HTTP_UNAUTHORIZED) {
            $event->setResponse(
                $this->createErrorResponse('Unauthenticated.', 'unauthenticated')
            );
        }

        if ($exception instanceof DomainExceptionInterface) {
            $event->setResponse(
                $this->createErrorResponse($exception->getMessage(), $exception->getErrorCode())
            );
        }

        if ($exception instanceof EntityNotFoundException) {
            $event->setResponse(
                $this->createErrorResponse($exception->getMessage(), $exception->getErrorCode(), 404)
            );
        }
    }

    private function createErrorResponse(
        string $message,
        string $errorCode,
        int $status = Response::HTTP_BAD_REQUEST
    ): JsonResponse {
        $response = new JsonResponse();
        $response->setData([
            'errors' => [
                [
                    'message' => $message,
                    'error_code' => $errorCode
                ]
            ]
        ]);
        $response->setStatusCode($status);

        return $response;
    }
}