<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\EventListener;

use App\Domain\Auth\ResetPasswordEvent;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

final class ResetPasswordEventListener
{
    private $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    public function __invoke(ResetPasswordEvent $event): void
    {
        $this->bus->dispatch(new Envelope($event));
    }
}
