<?php

declare(strict_types = 1);

namespace App\Infrastructure\Domain\EventListener;

use App\Domain\Service\Security\TokenValidator;
use App\Http\Api\Controller\TokenAuthenticatedController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class AuthenticatedActionListener
{
    private $tokenValidator;

    public function __construct(TokenValidator $tokenValidator)
    {
        $this->tokenValidator = $tokenValidator;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (! is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof TokenAuthenticatedController) {
            $token = $event->getRequest()->headers->get('Authorization');

            if ($token === null || ! $this->tokenValidator->validate($token)) {
                throw new AuthenticationException();
            }
        }
    }
}