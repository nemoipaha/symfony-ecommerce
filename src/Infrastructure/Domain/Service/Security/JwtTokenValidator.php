<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Service\Security;

use App\Domain\Exception\TokenExpiredException;
use App\Domain\Service\Security\TokenValidator;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;

final class JwtTokenValidator implements TokenValidator
{
    const PREFIX = 'Bearer';

    public function validate(string $token): bool
    {
        $prefix = self::PREFIX;
        $token = trim($token);

        if (preg_match("/{$prefix}\s\S+/", $token) !== 1) {
            return false;
        }

        $prefixLength = strlen(self::PREFIX) + 1;
        $token = substr($token, $prefixLength);
        $data = new ValidationData();
        $parser = new Parser();

        try {
            $tokenParsed = $parser->parse($token);
        } catch (\Exception $exception) {
            return false;
        }

        if ($tokenParsed->isExpired()) {
            throw new TokenExpiredException();
        }

        return $tokenParsed->validate($data);
    }
}