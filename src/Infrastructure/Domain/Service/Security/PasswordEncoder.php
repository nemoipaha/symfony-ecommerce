<?php

declare(strict_types = 1);

namespace App\Infrastructure\Domain\Service\Security;

use App\Domain\Service\Security\PasswordEncoderInterface;

class PasswordEncoder implements PasswordEncoderInterface
{
    public function encode(string $password): string
    {
        $encoded = password_hash($password, PASSWORD_ARGON2I);

        if ($encoded === false) {
            throw new \InvalidArgumentException('Cannot encode given value.');
        }

        return $encoded;
    }

    public function verify(string $passwordPlain, string $hash): bool
    {
        return password_verify($passwordPlain, $hash);
    }
}