<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\Service\Security;

use App\Domain\Service\Security\TokenGeneratorInterface;
use App\Domain\User\User;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Hmac\Sha512;
use Symfony\Component\DependencyInjection\Container;

final class JwtTokenGenerator implements TokenGeneratorInterface
{
    private $tokenBuilder;
    private $container;

    public function __construct(
        Builder $builder,
        Container $container
    ) {
        $this->tokenBuilder = $builder;
        $this->container = $container;
    }

    public function generate(User $user, int $expiration): string
    {
        $signer = new Sha512();
        // todo move into config repository
        $appKey = $this->container->getParameter('kernel.secret');
        $token = $this->tokenBuilder
            ->setExpiration($expiration)
            ->set('uuid', $user->getUid()->value())
            ->sign($signer, $appKey)
            ->getToken();

        return (string)$token;
    }
}