<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\MessageHandler;

use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class UserSignUpMessageHandler implements MessageHandlerInterface
{
    private $mailer;
    private $twig;
    private $dispatcher;

    public function __construct(
        \Swift_Mailer $mailer,
        EngineInterface $twig,
        EventDispatcherInterface $dispatcher
    ) {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->dispatcher = $dispatcher;
    }

    public function __invoke(UserSignUpMessage $event): void
    {
        $message = (new \Swift_Message('Welcome'))
            ->setTo($event->getEmail())
            ->setFrom(getenv('MAILER_FROM_EMAIL'))
            ->setBody(
                $this->twig->render(
                    'emails/registration.html.twig',
                    ['userId' => $event->getUserId()]
                ),
                'text/html'
            );

        if ($this->mailer->send($message) === 0) {
            throw new \RuntimeException('Send email error.');
        }

        $this->dispatcher->dispatch(Events::UNSPOOL);
    }
}
