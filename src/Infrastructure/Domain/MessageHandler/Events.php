<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\MessageHandler;

final class Events
{
    const UNSPOOL = 'unspool';
}
