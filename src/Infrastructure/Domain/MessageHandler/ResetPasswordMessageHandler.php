<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\MessageHandler;

use App\Domain\Auth\ResetPasswordEvent;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

final class ResetPasswordMessageHandler implements MessageHandlerInterface
{
    private $mailer;
    private $twig;
    private $dispatcher;
    private $router;

    public function __construct(
        \Swift_Mailer $mailer,
        EngineInterface $twig,
        EventDispatcherInterface $dispatcher,
        RouterInterface $router
    ) {
        $this->mailer = $mailer;
        $this->twig = $twig;
        $this->dispatcher = $dispatcher;
        $this->router = $router;
    }

    public function __invoke(ResetPasswordEvent $event): void
    {
        $url = $this->router->generate(
            'auth_reset_password',
            ['token' => $event->getPasswordToken()],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        $message = (new \Swift_Message('Reset password'))
            ->setTo($event->getEmail())
            ->setFrom(getenv('MAILER_FROM_EMAIL'))
            ->setBody(
                $this->twig->render(
                    'emails/reset_password.html.twig',
                    ['url' => $url]
                ),
                'text/html'
            );

        if ($this->mailer->send($message) === 0) {
            throw new \RuntimeException('Send email error.');
        }

        $this->dispatcher->dispatch(Events::UNSPOOL);
    }
}
