<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\MessageHandler;

final class UserSignUpMessage
{
    private $userId;
    private $email;

    public function __construct(string $userId, string $email)
    {
        $this->userId = $userId;
        $this->email = $email;
    }

    public function getUserId(): string
    {
        return $this->userId;
    }

    public function getEmail(): string
    {
        return $this->email;
    }
}
