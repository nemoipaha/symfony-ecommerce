<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class ControllerEventSubscriber implements EventSubscriberInterface
{
    public function processRequestAttributes(FilterControllerEvent $event): void
    {
        $request = $event->getRequest();

        if (empty($request->getContent())) {
            return;
        }

        $attributes = json_decode($request->getContent());

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new HttpException(400, 'Request content is broken.');
        }

        foreach ($attributes as $key => $attribute) {
            $request->attributes->set($key, $attribute);
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'processRequestAttributes'
        ];
    }
}
