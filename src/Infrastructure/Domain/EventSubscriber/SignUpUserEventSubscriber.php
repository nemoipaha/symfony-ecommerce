<?php

declare(strict_types=1);

namespace App\Infrastructure\Domain\EventSubscriber;

use App\Domain\User\UserSignedUpEvent;
use App\Infrastructure\Domain\MessageHandler\UserSignUpMessage;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

final class SignUpUserEventSubscriber implements EventSubscriberInterface
{
    private $bus;

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;
    }

    public static function getSubscribedEvents()
    {
        return [
            UserSignedUpEvent::NAME => [
                'sendRegistrationEmail'
            ]
        ];
    }

    public function sendRegistrationEmail(UserSignedUpEvent $event): void
    {
        $this->bus->dispatch(
            new Envelope(
                new UserSignUpMessage(
                    $event->getUser()->getUid()->value(),
                    $event->getUser()->getEmailAddress()->value()
                )
            )
        );
    }
}
