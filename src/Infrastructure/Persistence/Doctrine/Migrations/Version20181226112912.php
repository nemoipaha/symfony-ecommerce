<?php declare(strict_types=1);

namespace App\Infrastructure\Persistence\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181226112912 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $schema->getTable('users')->addUniqueIndex(['email'], 'email_index_uniq');
    }

    public function down(Schema $schema) : void
    {
        $schema->getTable('users')->dropIndex('email_index_uniq');
    }
}
