<?php declare(strict_types=1);

namespace App\Infrastructure\Persistence\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181225174528 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $schema->getTable('users')->setPrimaryKey(['id']);
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE users DROP CONSTRAINT users_pkey');
    }
}
