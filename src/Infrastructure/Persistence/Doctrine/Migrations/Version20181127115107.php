<?php declare(strict_types=1);

namespace App\Infrastructure\Persistence\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181127115107 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $table = $schema->createTable('users');
        $table->addColumn('id', 'uuid');
        $table->addColumn('name', 'string');
        $table->addColumn('password', 'string');
        $table->addColumn('email', 'string');
    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable('users');
    }
}
