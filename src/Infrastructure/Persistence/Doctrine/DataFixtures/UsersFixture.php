<?php

namespace App\Infrastructure\Persistence\Doctrine\DataFixtures;

use App\Domain\Service\Security\PasswordEncoderInterface;
use App\Domain\User\EmailAddress;
use App\Domain\User\Name;
use App\Domain\User\Password;
use App\Domain\User\User;
use App\Domain\User\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UsersFixture extends Fixture
{
    const USERS_COUNT = 5000;

    private $faker;
    private $passwordEncoder;
    private $userRepository;

    public function __construct(PasswordEncoderInterface $passwordEncoder, UserRepository $userRepository)
    {
        $this->faker = \Faker\Factory::create();
        $this->passwordEncoder = $passwordEncoder;
        $this->userRepository = $userRepository;
    }

    public function load(ObjectManager $manager)
    {
        $password = Password::create(
            $this->passwordEncoder->encode('secret')
        );

        for ($i = 0; $i < self::USERS_COUNT; $i++) {
            $user = new User(
                $this->userRepository->nextId(),
                Name::create($this->faker->unique()->name),
                $password,
                EmailAddress::fromString($this->faker->unique()->email)
            );

            $manager->persist($user);
        }

        $manager->flush();
    }
}
