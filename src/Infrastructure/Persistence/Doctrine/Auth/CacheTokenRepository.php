<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Doctrine\Auth;

use App\Domain\Auth\PasswordToken;
use App\Domain\Auth\PasswordTokenRepository;
use App\Domain\Exception\TokenExpiredException;
use App\Domain\User\EmailAddress;
use Psr\SimpleCache\CacheInterface;

final class CacheTokenRepository implements PasswordTokenRepository
{
    private $cache;

    const TOKEN_DATE_FORMAT = 'Y-m-d H:i:s';

    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    public function store(PasswordToken $passwordToken, int $seconds = self::EXPIRE_SECONDS): void
    {
        $this->cache->set(
            $this->createKey($passwordToken->getEmailAddress()),
            $this->serialize($passwordToken, $seconds),
            $seconds
        );
    }

    /**
     * @param EmailAddress $emailAddress
     * @return PasswordToken
     * @throws TokenExpiredException
     */
    public function findByEmail(EmailAddress $emailAddress): PasswordToken
    {
        $key = $this->createKey($emailAddress);

        if (! $this->cache->has($key)) {
            throw new TokenExpiredException();
        }

        return $this->deserialize($this->cache->get($key));
    }

    private function createExpiresDate(\DateTime $created, int $expires): \DateTime
    {
        return $created->add(new \DateInterval('PT' . $expires . 'S'));
    }

    private function createKey(EmailAddress $emailAddress): string
    {
        return sprintf('password_reset_%s', base64_encode($emailAddress->value()));
    }

    private function serialize(PasswordToken $token, int $seconds): array
    {
        $expiredDate = $this->createExpiresDate(new \DateTime(), $seconds);

        return [
            'user_email' => $token->getEmailAddress()->value(),
            'hash' => (string)$token,
            'expires_date' => $expiredDate->format(self::TOKEN_DATE_FORMAT)
        ];
    }

    private function deserialize(array $token): PasswordToken
    {
        return new PasswordToken(
            EmailAddress::fromString($token['user_email']),
            $token['hash']
        );
    }
}
