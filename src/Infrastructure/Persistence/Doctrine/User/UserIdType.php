<?php

declare(strict_types = 1);

namespace App\Infrastructure\Persistence\Doctrine\User;

use App\Domain\User\UserId;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Ramsey\Uuid\Doctrine\UuidType;

final class UserIdType extends UuidType
{
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $value = parent::convertToPHPValue($value, $platform);

        return UserId::fromString($value->toString());
    }
}