<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Doctrine\User;

use App\Domain\Exception\EntityNotFoundException;
use App\Domain\User\EmailAddress;
use App\Domain\User\User;
use App\Domain\User\UserId;
use App\Domain\User\UserRepository;
use Doctrine\Common\Cache\Cache;
use Doctrine\Common\Collections\ArrayCollection;

final class CacheUserRepositoryDecorator implements UserRepository
{
    /**
     * cache for 10 min
     */
    const EXPIRATION = 600;
    const USER_LIST_KEY = 'user_list';

    private $userRepository;
    private $cache;

    public function __construct(DoctrineUserRepository $userRepository, Cache $cache)
    {
        $this->userRepository = $userRepository;
        $this->cache = $cache;
    }

    /**
     * @param UserId $userId
     * @return User
     * @throws EntityNotFoundException
     */
    public function getById(UserId $userId): User
    {
        return $this->userRepository->getById($userId);
    }

    public function save(User $user): void
    {
        $this->userRepository->save($user);
        // todo flush cache
    }

    public function getAll(): ArrayCollection
    {
        if (! $this->cache->contains(self::USER_LIST_KEY)) {
            $this->cache->save(
                self::USER_LIST_KEY,
                $this->userRepository->getAll(),
                $this->getExpirationTime()
            );
        }

        return $this->cache->fetch(self::USER_LIST_KEY);
    }

    public function findByFilters(string $sort, string $order = 'asc', int $limit = 10): ArrayCollection
    {
        $key = $this->createKeyByFilters($sort, $order, $limit);

        if (! $this->cache->contains($key)) {
            $this->cache->save(
                $key,
                $this->userRepository->findByFilters($sort, $order, $limit),
                $this->getExpirationTime()
            );
        }

        return $this->cache->fetch($key);
    }

    private function getExpirationTime(): int
    {
        return self::EXPIRATION;
    }

    private function createKeyByFilters(string $sort, string $order = 'asc', int $limit = 10): string
    {
        return self::USER_LIST_KEY . '_' . implode('_', [$sort, $order, $limit]);
    }

    /**
     * @param EmailAddress $emailAddress
     * @return User
     * @throws EntityNotFoundException
     */
    public function findByEmail(EmailAddress $emailAddress): User
    {
        return $this->userRepository->findByEmail($emailAddress);
    }

    public function nextId(): UserId
    {
        return $this->userRepository->nextId();
    }

    public function emailIsUnique(EmailAddress $emailAddress): bool
    {
        return $this->userRepository->emailIsUnique($emailAddress);
    }

    public function delete(User $user): void
    {
        $this->userRepository->delete($user);
        // todo flush cache
    }
}
