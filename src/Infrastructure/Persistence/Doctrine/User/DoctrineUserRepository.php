<?php

declare(strict_types = 1);

namespace App\Infrastructure\Persistence\Doctrine\User;

use App\Domain\Exception\EntityNotFoundException;
use App\Domain\User\EmailAddress;
use App\Domain\User\User;
use App\Domain\User\UserId;
use App\Domain\User\UserRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Persistence\ManagerRegistry;
use Ramsey\Uuid\Uuid;

final class DoctrineUserRepository extends ServiceEntityRepository implements UserRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param UserId $userId
     * @return User
     * @throws EntityNotFoundException
     */
    public function getById(UserId $userId): User
    {
        $user = $this->find($userId->value());

        if (! $user) {
            throw new EntityNotFoundException();
        }

        return $user;
    }

    public function save(User $user): void
    {
        $this->getEntityManager()->merge($user);
        $this->getEntityManager()->flush();
    }

    public function getAll(): ArrayCollection
    {
        return new ArrayCollection($this->findAll());
    }

    public function findByFilters(string $sort, string $order = 'asc', int $limit = 10): ArrayCollection
    {
        $criteria = Criteria::create()
            ->orderBy([
                'email.value' => $order
            ])
            ->setMaxResults($limit);

        $data = $this->matching($criteria)->getValues();

        return new ArrayCollection($data);
    }

    /**
     * @param EmailAddress $emailAddress
     * @return User
     * @throws EntityNotFoundException
     */
    public function findByEmail(EmailAddress $emailAddress): User
    {
        $user = $this->findOneBy([
            'email.value' => $emailAddress->value()
        ]);

        if (! $user) {
            throw new EntityNotFoundException();
        }

        return $user;
    }

    public function nextId(): UserId
    {
        return UserId::fromString(Uuid::uuid4()->toString());
    }

    public function emailIsUnique(EmailAddress $emailAddress): bool
    {
        try {
            $this->findByEmail($emailAddress);

            return false;
        } catch (EntityNotFoundException $exception) {
            return true;
        }
    }

    public function delete(User $user): void
    {
        $this->getEntityManager()->remove($user);
        $this->getEntityManager()->flush();
    }
}