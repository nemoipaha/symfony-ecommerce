<?php

declare(strict_types = 1);

namespace App\Http\Api\Presenter;

use App\Domain\User\User;
use Doctrine\Common\Collections\ArrayCollection;

final class UserArrayPresenter
{
    public function present(User $user): array
    {
        return [
            'id' => $user->getUid()->value(),
            'name' => $user->getName()->value(),
            'email' => $user->getEmailAddress()->value()
        ];
    }

    public function presentCollection(ArrayCollection $users): array
    {
        return $users->map(
            function(User $user) {
                return $this->present($user);
            }
        )->getValues();
    }
}
