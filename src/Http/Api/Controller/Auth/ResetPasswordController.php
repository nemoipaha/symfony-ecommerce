<?php

declare(strict_types=1);

namespace App\Http\Api\Controller\Auth;

use App\Application\Command\ResetPassword\ResetPasswordCommand;
use App\Application\Command\ResetPassword\ResetPasswordCommandHandler;
use App\Http\Api\Controller\ApiController;
use Symfony\Component\HttpFoundation\Request;

final class ResetPasswordController extends ApiController
{
    private $resetPasswordCommandHandler;

    public function __construct(ResetPasswordCommandHandler $resetPasswordCommandHandler)
    {
        $this->resetPasswordCommandHandler = $resetPasswordCommandHandler;
    }

    public function __invoke(Request $request)
    {
        // @todo use validator
        $this->resetPasswordCommandHandler->execute(
            new ResetPasswordCommand(
                $request->attributes->get('email'),
                $request->attributes->get('token'),
                $request->attributes->get('password'),
                $request->attributes->get('password_confirmation')
            )
        );

        return $this->emptyResponse();
    }
}
