<?php

declare(strict_types = 1);

namespace App\Http\Api\Controller\Auth;

use App\Application\Command\SignUp\SignUpCommand;
use App\Application\Command\SignUp\SignUpCommandHandler;
use App\Http\Api\Controller\ApiController;
use Symfony\Component\HttpFoundation\Request;

class SignUpController extends ApiController
{
    public function __invoke(Request $request, SignUpCommandHandler $signUpCommandHandler)
    {
        $data = json_decode($request->getContent());

        $signUpCommandHandler->execute(
            new SignUpCommand(
                $data->name,
                $data->password,
                $data->email
            )
        );

        return $this->emptyResponse();
    }
}