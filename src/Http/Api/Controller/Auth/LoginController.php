<?php

declare(strict_types = 1);

namespace App\Http\Api\Controller\Auth;

use App\Application\Command\Login\LoginCommand;
use App\Application\Command\Login\LoginCommandHandler;
use App\Http\Api\Controller\ApiController;
use Symfony\Component\HttpFoundation\Request;

class LoginController extends ApiController
{
    public function __invoke(Request $request, LoginCommandHandler $loginCommandHandler)
    {
        $response = $loginCommandHandler->execute(
            new LoginCommand(
                $request->attributes->get('email'),
                $request->attributes->get('password')
            )
        );

        return $this->successResponse([
            'access_token' => $response->getToken(),
            'expires' => $response->getExpiration(),
            'token_type' => 'bearer'
        ]);
    }
}
