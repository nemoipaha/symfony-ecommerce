<?php

declare(strict_types=1);

namespace App\Http\Api\Controller\Auth;

use App\Application\Command\ForgotPassword\ForgotPasswordCommand;
use App\Application\Command\ForgotPassword\ForgotPasswordCommandHandler;
use App\Http\Api\Controller\ApiController;
use Symfony\Component\HttpFoundation\Request;

final class ForgotPasswordController extends ApiController
{
    public function __invoke(Request $request, ForgotPasswordCommandHandler $forgotPasswordCommandHandler)
    {
        $forgotPasswordCommandHandler->execute(
            new ForgotPasswordCommand($request->attributes->get('email'))
        );

        return $this->emptyResponse();
    }
}
