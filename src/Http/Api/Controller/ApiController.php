<?php

namespace App\Http\Api\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends AbstractController
{
    protected const SUCCESS_RESPONSE_ROOT_KEY = 'data';

    protected const ERROR_RESPONSE_ROOT_KEY = 'errors';

    final protected function successResponse(array $data, int $status = 200): JsonResponse
    {
        return $this->json([self::SUCCESS_RESPONSE_ROOT_KEY => $data], $status);
    }

    final protected function errorResponse(array $errors, int $status = 400): JsonResponse
    {
        return $this->json([self::ERROR_RESPONSE_ROOT_KEY => [$errors]], $status);
    }

    final protected function emptyResponse(): JsonResponse
    {
        return new JsonResponse();
    }
}
