<?php

namespace App\Http\Api\Controller;

use App\Application\Command\DeleteUser\DeleteUserCommand;
use App\Application\Command\DeleteUser\DeleteUserCommandHandler;
use App\Application\Command\UpdateUserProfile\UpdateUserProfileCommand;
use App\Application\Command\UpdateUserProfile\UpdateUserProfileCommandHandler;
use App\Application\Query\GetUserById\GetUserByIdQueryHandler;
use App\Application\Query\GetUserById\GetUserByIdQuery;
use App\Application\Query\GetUsers\GetUsersQuery;
use App\Application\Query\GetUsers\GetUsersQueryHandler;
use App\Http\Api\Presenter\UserArrayPresenter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UserController extends ApiController implements TokenAuthenticatedController
{
    public function queryUserById(
        Request $request,
        GetUserByIdQueryHandler $getUserByIdQueryHandler,
        UserArrayPresenter $userArrayPresenter
    ): JsonResponse {
        $response = $getUserByIdQueryHandler->execute(
            new GetUserByIdQuery($request->attributes->get('id'))
        );

        return $this->successResponse($userArrayPresenter->present($response->getUser()));
    }

    public function queryUsers(
        Request $request,
        UserArrayPresenter $userArrayPresenter,
        GetUsersQueryHandler $getUsersQueryHandler
    ): JsonResponse {
        $response = $getUsersQueryHandler->execute(
            new GetUsersQuery($request->query->get('limit'))
        );

        return $this->successResponse($userArrayPresenter->presentCollection($response->getUsers()));
    }

    public function updateUserProfile(
        Request $request,
        UserArrayPresenter $userArrayPresenter,
        UpdateUserProfileCommandHandler $updateUserProfileCommandHandler
    ): JsonResponse {
        $response = $updateUserProfileCommandHandler->execute(
            new UpdateUserProfileCommand(
                $request->attributes->get('id'),
                $request->attributes->get('email'),
                $request->attributes->get('name')
            )
        );

        return $this->successResponse($userArrayPresenter->present($response->getUser()));
    }

    public function deleteUser(Request $request, DeleteUserCommandHandler $deleteUserCommandHandler): JsonResponse
    {
        $deleteUserCommandHandler->execute(
            new DeleteUserCommand($request->attributes->get('id'))
        );

        return $this->emptyResponse();
    }
}
