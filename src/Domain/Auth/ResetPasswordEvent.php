<?php

declare(strict_types=1);

namespace App\Domain\Auth;

use Symfony\Component\EventDispatcher\Event;

final class ResetPasswordEvent extends Event
{
    const NAME = 'auth.reset_password';

    private $passwordToken;
    private $email;

    public function __construct(string $passwordToken, string $email)
    {
        $this->passwordToken = $passwordToken;
        $this->email = $email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPasswordToken(): string
    {
        return $this->passwordToken;
    }
}
