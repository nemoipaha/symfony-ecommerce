<?php

declare(strict_types=1);

namespace App\Domain\Auth;

use App\Domain\Exception\DomainExceptionInterface;

final class PasswordConfirmationInvalid extends \DomainException implements DomainExceptionInterface
{
    /**
     * Returns unique error code
     *
     * @return string
     */
    public function getErrorCode(): string
    {
        return 'passwords_should_match';
    }

    public static function make(): self
    {
        return new static('Passwords don\'t match.');
    }
}
