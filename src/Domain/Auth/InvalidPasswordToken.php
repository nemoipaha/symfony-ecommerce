<?php

declare(strict_types=1);

namespace App\Domain\Auth;

use App\Domain\Exception\DomainExceptionInterface;

final class InvalidPasswordToken extends \DomainException implements DomainExceptionInterface
{
    /**
     * Returns unique error code
     *
     * @return string
     */
    public function getErrorCode(): string
    {
        return 'invalid_token';
    }

    public static function make(): self
    {
        return new self('Token is invalid.');
    }
}
