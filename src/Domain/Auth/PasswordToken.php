<?php

declare(strict_types=1);

namespace App\Domain\Auth;

use App\Domain\User\EmailAddress;
use App\Domain\ValueObject\StringValue;

final class PasswordToken extends StringValue
{
    private $emailAddress;

    public function __construct(EmailAddress $emailAddress, string $hash)
    {
        $this->value = $hash;
        $this->emailAddress = $emailAddress;
    }

    /**
     * Generate length * 2 hash string
     *
     * @param int $length
     * @return string
     * @throws \Exception
     */
    public static function generate(EmailAddress $emailAddress, int $length = 16): self
    {
        return new self(
            $emailAddress,
            hash('sha256', bin2hex(random_bytes($length)))
        );
    }

    public function getEmailAddress(): EmailAddress
    {
        return $this->emailAddress;
    }

    public function isHashMatches(string $hash): bool
    {
        return $this->value() === $hash;
    }
}
