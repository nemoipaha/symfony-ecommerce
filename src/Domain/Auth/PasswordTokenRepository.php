<?php

declare(strict_types=1);

namespace App\Domain\Auth;

use App\Domain\Exception\TokenExpiredException;
use App\Domain\User\EmailAddress;

interface PasswordTokenRepository
{
    const EXPIRE_SECONDS = 7200;

    public function store(PasswordToken $passwordToken, int $seconds = self::EXPIRE_SECONDS): void;

    /**
     * @param EmailAddress $emailAddress
     * @return PasswordToken
     * @throws TokenExpiredException
     */
    public function findByEmail(EmailAddress $emailAddress): PasswordToken;
}
