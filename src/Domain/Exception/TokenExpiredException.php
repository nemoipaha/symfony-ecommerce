<?php

declare(strict_types=1);

namespace App\Domain\Exception;

use Throwable;

class TokenExpiredException extends \RuntimeException implements DomainExceptionInterface
{
    const MESSAGE = 'Token has expired.';

    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct(self::MESSAGE, $code, $previous);
    }

    /**
     * Returns unique error code
     *
     * @return string
     */
    public function getErrorCode(): string
    {
        return 'token_expired';
    }
}