<?php

namespace App\Domain\Exception;

class InvalidUserCredentialsException extends \RuntimeException implements DomainExceptionInterface
{
    /**
     * Returns unique error code
     *
     * @return string
     */
    public function getErrorCode(): string
    {
        return 'invalid_credentials';
    }

    public static function create(): self
    {
        return new static('Invalid credentials provided.');
    }
}