<?php

declare(strict_types = 1);

namespace App\Domain\Exception;

interface DomainExceptionInterface
{
    /**
     * Returns unique error code
     *
     * @return string
     */
    public function getErrorCode(): string;
}