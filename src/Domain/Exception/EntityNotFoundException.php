<?php

declare(strict_types = 1);

namespace App\Domain\Exception;

class EntityNotFoundException extends \DomainException implements DomainExceptionInterface
{
    private $entityType;
    private $entityId;

    public function getEntityType(): string
    {
        return $this->entityType;
    }

    public function getEntityId(): string
    {
        return $this->entityId;
    }

    public static function forTypeAndId(string $entityType, string $id): self
    {
        $exception = new static("Entity {$entityType} with id {$id} not found.");
        $exception->entityType = $entityType;
        $exception->entityId = $id;

        return $exception;
    }

    /**
     * Returns unique error code
     *
     * @return string
     */
    public function getErrorCode(): string
    {
        return 'not_found';
    }
}
