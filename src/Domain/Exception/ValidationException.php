<?php

declare(strict_types = 1);

namespace App\Domain\Exception;

use Throwable;

class ValidationException extends \RuntimeException implements DomainExceptionInterface
{
    private $errorCode;

    public function __construct(string $message, string $errorCode, Throwable $previous = null)
    {
        parent::__construct($message, 0, $previous);

        $this->errorCode = $errorCode;
    }

    public function getErrorCode(): string
    {
        return $this->errorCode;
    }

    public static function invalidValue(string $message): self
    {
        return new static($message, 'invalid_field');
    }
}