<?php

declare(strict_types = 1);

namespace App\Domain\ValueObject;

use Assert\Assertion;
use Assert\AssertionFailedException;
use InvalidArgumentException;

final class Limit
{
    private const DEFAULT = 15;

    private $value;

    public function __construct(int $value)
    {
        if ($value <= 0) {
            throw new InvalidArgumentException('Limit must be >= 0.');
        }

        $this->value = $value;
    }

    public function value(): int
    {
        return $this->value;
    }

    public static function fromString(string $limit): self
    {
        try {
            Assertion::digit($limit);
        } catch (AssertionFailedException $ex) {
            throw new InvalidArgumentException('Limit must be integer.');
        }

        return new self((int)$limit);
    }

    public static function default(): self
    {
        return new self(self::DEFAULT);
    }
}
