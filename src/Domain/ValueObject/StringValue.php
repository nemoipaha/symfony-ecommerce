<?php

namespace App\Domain\ValueObject;

abstract class StringValue
{
    protected $value;

    public function value(): string
    {
        return $this->value;
    }

    public function __toString()
    {
        return $this->value();
    }

    public function equals(StringValue $stringValue): bool
    {
        return $this->value() === $stringValue->value();
    }
}
