<?php

declare(strict_types = 1);

namespace App\Domain\User;

use App\Domain\Service\Security\PasswordEncoderInterface;

final class User
{
    private $uid;
    private $name;
    private $password;
    private $email;

    public function __construct(
        UserId $userId,
        Name $name,
        Password $password,
        EmailAddress $emailAddress
    ) {
        $this->uid = $userId;
        $this->name = $name;
        $this->password = $password;
        $this->email = $emailAddress;
    }

    public function getUid(): UserId
    {
        return $this->uid;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getPassword(): Password
    {
        return $this->password;
    }

    public function getEmailAddress(): EmailAddress
    {
        return $this->email;
    }

    public function withName(Name $name): self
    {
        $user = clone $this;
        $user->name = $name;

        return $user;
    }

    public function withPassword(Password $password): self
    {
        $user = clone $this;
        $user->password = $password;

        return $user;
    }

    public function withEmail(EmailAddress $email): self
    {
        $user = clone $this;
        $user->email = $email;

        return $user;
    }

    public function verifyPassword(PasswordEncoderInterface $passwordEncoder, string $plainPassword): bool
    {
        return $passwordEncoder->verify($plainPassword, $this->getPassword()->value());
    }
}
