<?php

declare(strict_types = 1);

namespace App\Domain\User;

use App\Domain\ValueObject\StringValue;

final class Name extends StringValue
{
    private function __construct(string $name)
    {
        if (empty($name)) {
            throw new \InvalidArgumentException("Name cannot be empty.");
        }

        $this->value = $name;
    }

    public static function create(string $name): self
    {
        return new self($name);
    }
}
