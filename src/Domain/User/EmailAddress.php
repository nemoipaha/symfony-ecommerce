<?php

declare(strict_types = 1);

namespace App\Domain\User;

use App\Domain\Exception\ValidationException;
use App\Domain\ValueObject\StringValue;
use Assert\Assertion;
use Assert\AssertionFailedException;

final class EmailAddress extends StringValue
{
    private function __construct(string $email)
    {
        try {
            Assertion::email($email);
        } catch (AssertionFailedException $ex) {
            throw ValidationException::invalidValue('Email format is invalid.');
        }

        $this->value = $email;
    }

    public static function fromString(string $value): self
    {
        return new self($value);
    }
}