<?php

declare(strict_types = 1);

namespace App\Domain\User;

use App\Domain\Exception\EntityNotFoundException;
use Doctrine\Common\Collections\ArrayCollection;

interface UserRepository
{
    /**
     * @param UserId $userId
     * @return User
     * @throws EntityNotFoundException
     */
    public function getById(UserId $userId): User;

    public function save(User $user): void;

    public function getAll(): ArrayCollection;

    public function findByFilters(string $sort, string $order = 'asc', int $limit = 10): ArrayCollection;

    /**
     * @param EmailAddress $emailAddress
     * @return User
     * @throws EntityNotFoundException
     */
    public function findByEmail(EmailAddress $emailAddress): User;

    public function nextId(): UserId;

    public function emailIsUnique(EmailAddress $emailAddress): bool;

    public function delete(User $user): void;
}
