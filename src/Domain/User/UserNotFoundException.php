<?php

declare(strict_types = 1);

namespace App\Domain\User;

use App\Domain\Exception\EntityNotFoundException;

class UserNotFoundException extends EntityNotFoundException
{
    public static function forId(string $userId): self
    {
        return new static("User with id {$userId} not found.");
    }

    public static function forEmail(string $email): self
    {
        return new static("User with email {$email} not found.");
    }
}
