<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Domain\Exception\DomainExceptionInterface;

class EmailAlreadyTakenException extends \DomainException implements DomainExceptionInterface
{
    /**
     * Returns unique error code
     *
     * @return string
     */
    public function getErrorCode(): string
    {
        return 'email_not_unique';
    }

    public static function forEmail(string $email): self
    {
        return new static("Email {$email} is already taken.");
    }
}