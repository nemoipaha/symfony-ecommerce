<?php

declare(strict_types=1);

namespace App\Domain\User;

use Symfony\Component\EventDispatcher\Event;

final class UserSignedUpEvent extends Event
{
    const NAME = 'user.signup';

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
