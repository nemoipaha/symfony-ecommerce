<?php

declare(strict_types = 1);

namespace App\Domain\User;

use App\Domain\ValueObject\StringValue;
use Ramsey\Uuid\Uuid;

final class UserId extends StringValue
{
    private function __construct(string $value)
    {
        $this->guard($value);

        $this->value = $value;
    }

    private function guard(string $id): void
    {
        if (! Uuid::isValid($id)) {
            throw new \InvalidArgumentException("Invalid id value {$id}.");
        }
    }

    public static function fromString(string $uid): self
    {
        return new self($uid);
    }
}
