<?php

declare(strict_types = 1);

namespace App\Domain\User;

use App\Domain\Service\Security\PasswordEncoderInterface;
use App\Domain\ValueObject\StringValue;

final class Password extends StringValue
{
    private function __construct(string $hash)
    {
        // @todo password regex validation
        if (empty($hash)) {
            throw new \InvalidArgumentException('Password cannot be empty.');
        }

        $this->value = $hash;
    }

    public static function create(string $hash): self
    {
        return new self($hash);
    }

    public static function generate(PasswordEncoderInterface $passwordEncoder, string $plainPassword): self
    {
        return new self($passwordEncoder->encode($plainPassword));
    }
}
