<?php

declare(strict_types = 1);

namespace App\Domain\Service\Security;

interface PasswordEncoderInterface
{
    /**
     * @param string $password
     * @return string
     * @throws \InvalidArgumentException
     */
    public function encode(string $password): string;

    /**
     * @param string $passwordPlain
     * @param string $hash
     * @return bool
     */
    public function verify(string $passwordPlain, string $hash): bool;
}