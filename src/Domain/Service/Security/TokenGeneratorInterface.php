<?php

declare(strict_types=1);

namespace App\Domain\Service\Security;

use App\Domain\User\User;

interface TokenGeneratorInterface
{
    public function generate(User $user, int $expiration): string;
}