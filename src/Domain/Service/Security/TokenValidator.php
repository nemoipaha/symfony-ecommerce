<?php

declare(strict_types=1);

namespace App\Domain\Service\Security;

use App\Domain\Exception\TokenExpiredException;

interface TokenValidator
{
    /**
     * @param string $token
     * @return bool
     * @throws TokenExpiredException
     */
    public function validate(string $token): bool;
}